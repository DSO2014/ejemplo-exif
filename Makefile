# ejemplo de uso FUSE para DSO 0
# hemos instalado el paquete de desarrollo de FUSE con:
# sudo apt-get install fuse-dev
# hemos instalado el paquete de desarrollo de EXIF con:
# sudo apt-get install libexif-dev

fuse_flags= -g -D_FILE_OFFSET_BITS=64 -lfuse -pthread
exif_flags= -l exif

.PHONY : mount umount test compila

compila : ejemplo_exif photographer

ejemplo_exif : ejemplo_exif.c
	gcc  -o $@ $^ ${fuse_flags} ${exif_flags}
	mkdir -p punto_montaje

photographer : photographer.c
	gcc $^ -o $@ ${exif_flags}

test:
	./photographer imagenes/Cookie_monster.jpg

mount : 
	./ejemplo_exif imagenes punto_montaje
debug :
	./ejemplo_exif -d imagenes punto_montaje 
umount :
	fusermount -u punto_montaje

