# README #

### What is this repository for? ###

* Ejemplo de aplicación fuse que permite leer ficheros de imagenes en un directorio y clasificarlos según etiquetas EXIF
* Prestar atención a cómo se toma el path absoluto del directorio con la función realpath(), para poder abrir después los ficheros originales
* He añadido el ejemplo photographer.c de la librería. Se puede compiar con "make" y probar con "make test"
* El ejemplo de fuse se ejecuta con "make mount" y usa el directorio imagenes que tiene 4 ficheros jpg. Dos de ellos tienen la etiqueta "favorita" en el TAG de comentario (EXIF_TAG_USER_COMMENT).
* El ejemplo pone todas las imagenes accesibles en el directorio "todos" y sólo las que tienen la etiqueta "favorita" en el directorio "favoritos"
* Se han utilizado las funciones de C para leer los contenidos del directorio
* Se han utilizado las funciones de libexif para comprobar si la imagen tenía la etiqueta requerida para estar en el directorio "favoritos"
* Cuando un fichero de imagen no contine etiquetas EXIF la función exif_data_new_from_file() devuelve NULL
* Hay que instalar la librería con "sudo apt-get install libexif-dev"

### How do I get set up? ###

* Usar la herramienta make
* hay reglas para compilar, montar, depurar, etc. (ejemplo_exif, mount, umount, debug)

### Who do I talk to? ###

* Andrés Rodríguez <andres at uma dot es>
