/*
  FUSE: Filesystem in Userspace
*/

#define FUSE_USE_VERSION 26

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <malloc.h>
#include <unistd.h>
#include <stdlib.h>
#include <limits.h>
#include <ctype.h>

#include <sys/types.h>
#include <dirent.h>
#include <libgen.h>

#include <libexif/exif-data.h>  // librería instalada con sudo apt-get install libexif-dev


struct structura_mis_datos
{
    char * path;							/* path completo del directorio inicial */
    char * directorio;
    struct timespec st_atim;  				/* fechas del fichero */
    struct timespec st_mtim;
    struct timespec st_ctim;
    unsigned long st_size;
    uid_t     st_uid;        				/* El usuario y grupo */
    gid_t     st_gid;
};

int es_favorito(const char * camino)
{
    ExifData *ed;
    ExifEntry *entry;
    ed = exif_data_new_from_file(camino);
    if (ed) {
        fprintf(stderr,">> Tiene exif tag: %s\n",camino);
        entry = exif_content_get_entry(ed->ifd[EXIF_IFD_EXIF], EXIF_TAG_USER_COMMENT);
        if (entry) {
            char buffer[64];
            if (exif_entry_get_value(entry, buffer, sizeof(buffer))) {
                fprintf(stderr,">> comment tag : %s\n",buffer);

                // trim_spaces(buf);
                if(strcmp(buffer,"favorito")==0 || strcmp(buffer,"favorita")==0  )  return 1;
            }
        }
    }
    return 0;
}

int existe_fichero(const char * nombre, const char *path)
{
    DIR *dir;
    struct dirent *dp;
    char * file_name;
    dir = opendir(path);
    while ((dp=readdir(dir)) != NULL)
        if ( !strcmp(dp->d_name, nombre))  {
            closedir(dir);
            return 1;
        }
    closedir(dir);
    return 0;

}

int existe_fichero_con_condicion(const char * nombre, const char *path, int (*cumple)(const char *))
{
    DIR *dir;
    struct dirent *dp;
    char * file_name;
    dir = opendir(path);
    while ((dp=readdir(dir)) != NULL)
        if ( !strcmp(dp->d_name, nombre) )  {
            char camino[512]; // fichero en el path original
            strcpy(camino,path);
            strcat(camino,"/");
            strcat(camino,dp->d_name);
            closedir(dir);
            if(cumple(camino)) return 1;
            else return 0;
        }
    closedir(dir);
    return 0;

}


static int ejemplo_getattr(const char *path, struct stat *stbuf)
{
    int res = 0;
    struct structura_mis_datos* mis_datos = (struct structura_mis_datos*)fuse_get_context()->private_data;
    char lpath[512];
    char * filename= basename((char *)path);
    strcpy(lpath,mis_datos->path);
    strcat(lpath,"/");
    strcat(lpath,filename);
   
    memset(stbuf, 0, sizeof(struct stat));
    
    fprintf(stderr," nombre de fichero: %s\n",filename);

    if (strcmp(path, "/") == 0) {  // dir raiz
        stbuf->st_mode = S_IFDIR | 0755;
        stbuf->st_nlink = 4; //numero de entradas en sistema de ficheros (directorios por defecto 2 -> . y el ..)
        stbuf->st_uid = mis_datos->st_uid; //propietario del fichero
        stbuf->st_gid = mis_datos->st_gid; // grupo propietario del fichero

        stbuf->st_atime = mis_datos->st_atime; //horas de modificación etc ...
        stbuf->st_mtime = mis_datos->st_mtime;
        stbuf->st_ctime = mis_datos->st_ctime;
        stbuf->st_size = 4096; //tamaño
        stbuf->st_blocks = stbuf->st_size / 512 + (stbuf->st_size % 512) ? 1 : 0; // tamaño divido entre 512
    }
    else if ( (strcmp(path+1, "favoritos") == 0) ) // fichero
    {
        stbuf->st_mode = S_IFDIR | 0755;
        stbuf->st_nlink = 2; //numero de entradas en sistema de ficheros (directorios por defecto 2 -> . y el ..)
        stbuf->st_uid = mis_datos->st_uid; //propietario del fichero
        stbuf->st_gid = mis_datos->st_gid; // grupo propietario del fichero

        stbuf->st_atime = mis_datos->st_atime; //horas de modificación etc ...
        stbuf->st_mtime = mis_datos->st_mtime;
        stbuf->st_ctime = mis_datos->st_ctime;
        stbuf->st_size = 4096; //tamaño
        stbuf->st_blocks = stbuf->st_size / 512 + (stbuf->st_size % 512) ? 1 : 0; // tamaño divido entre 512

    }
    else if ( (strcmp(path+1, "todos") == 0) ) // fichero
    {
        stbuf->st_mode = S_IFDIR | 0755;
        stbuf->st_nlink = 2; //numero de entradas en sistema de ficheros (directorios por defecto 2 -> . y el ..)
        stbuf->st_uid = mis_datos->st_uid; //propietario del fichero
        stbuf->st_gid = mis_datos->st_gid; // grupo propietario del fichero

        stbuf->st_atime = mis_datos->st_atime; //horas de modificación etc ...
        stbuf->st_mtime = mis_datos->st_mtime;
        stbuf->st_ctime = mis_datos->st_ctime;
        stbuf->st_size = 4096; //tamaño
        stbuf->st_blocks = stbuf->st_size / 512 + (stbuf->st_size % 512) ? 1 : 0; // tamaño divido entre 512

    }
    else if ( (strncmp(path+1, "todos/",6)==0      && existe_fichero(filename, mis_datos->path)) ||
              (strncmp(path+1, "favoritos/",10)==0 && existe_fichero_con_condicion(filename, mis_datos->path, es_favorito))
            ) // fichero
    {
        struct stat  fileStat;
        stat(lpath, &fileStat);
        
        stbuf->st_mode = S_IFREG | 0444;
        stbuf->st_nlink = 1; //numero de entradas en sistema de ficheros (directorios por defecto 2 -> . y el ..)

        stbuf->st_uid = fileStat.st_uid; //propietario del fichero
        stbuf->st_gid = fileStat.st_gid; // grupo propietario del fichero
        stbuf->st_atime = fileStat.st_atime; //horas de modificación etc ...
        stbuf->st_mtime = fileStat.st_mtime;
        stbuf->st_ctime = fileStat.st_ctime;
        stbuf->st_size = fileStat.st_size; //tamaño
        stbuf->st_blocks = stbuf->st_size / 512 + (stbuf->st_size % 512) ? 1 : 0; // tamaño divido entre 512

    } else
        res = -ENOENT;

    return res;
}

static int ejemplo_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                           off_t offset, struct fuse_file_info *fi)
{
    struct structura_mis_datos* mis_datos = (struct structura_mis_datos*)fuse_get_context()->private_data;
    
    if (strcmp(path, "/") == 0)
    {
        if(filler(buf, ".", NULL, 0)!=0) return -ENOMEM;
        if(filler(buf, "..", NULL, 0)!=0) return -ENOMEM;
        if(filler(buf, "favoritos", NULL, 0)!=0) return -ENOMEM;
        if(filler(buf, "todos", NULL, 0)!=0) return -ENOMEM;
    }

    else  if (strcmp(path, "/todos") == 0)
    {
        if(filler(buf, ".", NULL, 0)!=0) return -ENOMEM;
        if(filler(buf, "..", NULL, 0)!=0) return -ENOMEM;
        DIR *dir;
        struct dirent *dp;
        char * file_name;
        dir = opendir(mis_datos->path);
        while ((dp=readdir(dir)) != NULL) {
            if ( !strcmp(dp->d_name, ".") || !strcmp(dp->d_name, "..") )
            {
                // do nothing (straight logic)
            } else {
                file_name = dp->d_name; // use it
                if(filler(buf, file_name, NULL, 0)!=0) return -ENOMEM;
            }
        }
        closedir(dir);
    }
    else  if (strcmp(path, "/favoritos") == 0)
    {
        if(filler(buf, ".", NULL, 0)!=0) return -ENOMEM;
        if(filler(buf, "..", NULL, 0)!=0) return -ENOMEM;
        DIR *dir;
        struct dirent *dp;
        char * file_name;
        dir = opendir(mis_datos->path);
        while ((dp=readdir(dir)) != NULL) {
            if ( !strcmp(dp->d_name, ".") || !strcmp(dp->d_name, "..") )
            {
                // do nothing (straight logic)
            } else {
                char camino[512]; // fichero en el path original
                file_name = dp->d_name; // use it
                strcpy(camino,mis_datos->path);
                strcat(camino,"/");
                strcat(camino,file_name);
                if(es_favorito(camino))
                    if(filler(buf, file_name, NULL, 0)!=0) return -ENOMEM;

            }
        }
        closedir(dir);
    }

    else {
        return -ENOMEM;
    }
    return 0;
}

static int ejemplo_open(const char *path, struct fuse_file_info *fi)
{
    struct structura_mis_datos* mis_datos = (struct structura_mis_datos*)fuse_get_context()->private_data;
    
    char lpath[512];
    char * filename= basename((char *)path);
    strcpy(lpath,mis_datos->path);
    strcat(lpath,"/");
    strcat(lpath,filename);

    if (existe_fichero(filename, mis_datos->path)) // fichero
    {
        if ((fi->flags & 3) != O_RDONLY) return -EACCES;
        fi->fh=open(lpath,fi->flags);
        if(fi->fh<0) return -errno;
    }
    else
        return -ENOENT;

    return 0;
}

static int ejemplo_read(const char *path, char *buf, size_t size, off_t offset,
                        struct fuse_file_info *fi)
{
    return pread(fi->fh, buf, size, offset);
}

int ejemplo_release(const char *path, struct fuse_file_info *fi)
{
    return close(fi->fh);
}

static struct fuse_operations ejemplo_oper = {
    .getattr	= ejemplo_getattr,
    .readdir	= ejemplo_readdir,
    .open		= ejemplo_open,
    .read		= ejemplo_read,
    .release	= ejemplo_release
};

int main(int argc, char *argv[])
{
    struct structura_mis_datos* mis_datos;
    mis_datos = (struct structura_mis_datos*) malloc(sizeof(struct structura_mis_datos));
    FILE *f;
    struct stat fileStat;

    // análisis parámetros de entrada
    if ((argc < 3) || (argv[argc - 2][0] == '-') || (argv[argc - 1][0] == '-'))
    {
        perror("Parametros insuficientes");
        exit(-1);
    }

    mis_datos->directorio = strdup(argv[argc - 2]); // fichero original
    argv[argc - 2] = argv[argc - 1];
    argv[argc - 1] = NULL;
    argc--;

    // leer metadatos del fichero
    mis_datos->path = realpath(mis_datos->directorio, NULL);
    fprintf(stderr,"real path: %s\n",mis_datos->path);

    stat(mis_datos->path, &fileStat);
    mis_datos->st_uid= fileStat.st_uid;
    mis_datos->st_gid= fileStat.st_gid;
    mis_datos->st_atime = fileStat.st_atime;
    mis_datos->st_ctime = fileStat.st_ctime;
    mis_datos->st_mtime = fileStat.st_mtime;
    mis_datos->st_size = fileStat.st_size;

    return fuse_main(argc, argv, &ejemplo_oper, mis_datos);
}
